const pluralize = require('pluralize');

/**
 * Print report header containing top-level
 * stats about the test run
 * @param results
 */
const printReportHeader = function (results) {
  const { numFailedTests: failedTests, numPassedTests, numTotalTests, numTotalTestSuites} = results;

  if (failedTests === 0) {
    return {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `@here :tada: All ${numPassedTests} of ${numTotalTests}  ${pluralize(
          'test',
          numTotalTests,
        )} passed for Testing Project ${process.env.PROJECT.toUpperCase()} in ${process.env.STAGE.toUpperCase()} environment!`,
      },
    };
  }

  return {
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `@here :boom: ${failedTests} of ${numTotalTests} ${pluralize(
        'test',
        numTotalTests,
      )} failed for Testing Project ${process.env.PROJECT.toUpperCase()} in ${process.env.STAGE.toUpperCase()} environment!`,
    },
  };
};

/**
 * Print results of every suite
 * @param results
 */
const printSuiteResults = function (results) {
  const blocks = [];

  for (const suite of results.testResults) {
    blocks.push(
      {
        type: 'divider',
      },
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `Test Suite \`${suite.testResults[0].ancestorTitles[0]}\`: \n\n🚨 ${suite.numFailingTests} failing, ✅ ${suite.numPassingTests} passing`,
        },
      },
    );

    // Print complete failure message for failed test suites
    if (suite.failureMessage) {
      blocks.push({
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `
    🚧 Suite failed with message: \`\`\`
    ${suite.failureMessage.replace(
      // Strip ANSI color codes from failure message
      /[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g,
      '',
    )} \`\`\``,
        },
      });
    }

    // blocks.push(...suite.testResults.flatMap(printTestResults));
  }

  return blocks;
};

module.exports.generateSlackMessage = function (results, s3link) {
  const { endTime, startTime } = results;
  return {
    blocks: [
      printReportHeader(results),
      //   ...printSuiteResults(results),
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `Test report downloadable from <${s3link}|report.html> for 14 days!`,
        },
      },
      {
        type: 'divider',
      },
      {
        type: 'context',
        elements: [
          {
            type: 'mrkdwn',
            text: `Report generated at ${new Date().toLocaleString('en-US')} over ${(endTime - startTime)/1000} seconds.`,
          },
        ],
      },
    ],
  };
};
