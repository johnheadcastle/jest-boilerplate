const jest = require('jest');
const fs = require('fs');
const { IncomingWebhook } = require('@slack/webhook');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();

const { generateSlackMessage } = require('./slack');

console.log(`Cold starting test handler ${process.env.AWS_LAMBDA_FUNCTION_NAME} in ${process.env.AWS_REGION}`);

module.exports.handler = async (event, context) => {
  console.log('Executing Tests');
  await jest.run([`--selectProjects=${process.env.PROJECT}`, '--json', '--outputFile=/tmp/results.json']);

  const reportFileName = '/tmp/report.html';
  const reportContent = fs.readFileSync(reportFileName);
  const resultsFileName = '/tmp/results.json';
  const resultsContent = fs.readFileSync(resultsFileName);
  const results = JSON.parse(resultsContent);
  const reportKey = `reports/report-${Math.floor(new Date().getTime() / 1000)}.html`;
  const resultsKey = `results/results-${Math.floor(new Date().getTime() / 1000)}.json`;

  // upload report to s3
  await s3
    .upload({
      Bucket: process.env.BUCKETNAME,
      Key: reportKey,
      Body: reportContent,
    })
    .promise();

  // upload raw results to s3
  await s3
    .upload({
      Bucket: process.env.BUCKETNAME,
      Key: resultsKey,
      Body: resultsContent,
    })
    .promise();

  const s3presignedUrl = s3.getSignedUrl('getObject', {
    Bucket: process.env.BUCKETNAME,
    Key: reportKey,
    Expires: 60 * 60 * 24 * 14, // 14 Days
  });

  const url = process.env.SLACK_WEBHOOK_URL;
  if (url) {
    const slackWebhook = new IncomingWebhook(url);
    const slackMessage = generateSlackMessage(results, s3presignedUrl);
    await slackWebhook.send(slackMessage);
  }

  return 'Testing Completed';
};
