describe('Addition Test', () => {
  test('multiplication 1 x 2 to equal 2', () => {
    expect(1 * 2, 'Vales must equal 2').toBe(2);
  });

  test('adds 2 x 2 to equal 4', () => {
    expect(2 * 2, 'Vales must equal 4').toBe(5);
  });
});
