import type { Config } from '@jest/types';

const reportDir = process.env.STAGE !== 'docker' ? '/tmp' : './report';
const reportName = process.env.STAGE !== 'docker' ? 'report.html' : `report-${Math.floor(new Date().getTime() / 1000)}.html`;

const config: Config.InitialOptions = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  // roots: ["<rootDir>/src/", "<rootDir>/tests/"]

  // reporters: ['default', 'jest-html-reporters', '<rootDir>/dist/reporter/slackreporter.js'],
  reporters: [
    // 'default',
    [
      'jest-html-reporters',
      {
        publicPath: reportDir,
        filename: reportName,
        // expand: true,
        // openReport: true,
      },
    ],
    '<rootDir>/reporter/slackreporter.js',
  ],

  projects: [
    {
      displayName: 'default',
      setupFilesAfterEnv: ['@alex_neo/jest-expect-message'],
      // roots: ['<rootDir>/src/default'],
    },
    {
      displayName: 'api',
      // runner: 'jest-runner-eslint',
      // testMatch: ['<rootDir>/**/*.js'],
      setupFilesAfterEnv: ['@alex_neo/jest-expect-message'],
      // roots: ['<rootDir>/src/api'],
    },
    // {
    //   displayName: 'lint',
    //   runner: 'jest-runner-eslint',
    //   setupFilesAfterEnv: ['@alex_neo/jest-expect-message'],
    //   roots: ['<rootDir>/src'],
    // },
  ],
};

export default config;
