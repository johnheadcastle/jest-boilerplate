// src/tests/reporter.ts
// Look at https://brunoscheufler.com/blog/2020-02-14-supercharging-jest-with-custom-reporters
// Those two packages supply the types we need to
// build our custom reporter

import { Context, Reporter } from '@jest/reporters';

import { AggregatedResult } from '@jest/test-result';

// Our reporter implements only the onRunComplete lifecycle
// function, run after all tests have completed
export default class CustomReporter implements Pick<Reporter, 'onRunComplete'> {
  async onRunComplete(_: Set<Context>, results: AggregatedResult) {
    // TODO Add Slack webhook trigger
    console.log('Your report is available!');
  }
}