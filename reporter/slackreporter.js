// src/tests/reporter.ts
// Look at https://brunoscheufler.com/blog/2020-02-14-supercharging-jest-with-custom-reporters
// Those two packages supply the types we need to
// build our custom reporter

// numFailedTestSuites: 6,
// numFailedTests: 6,
// numPassedTestSuites: 0,
// numPassedTests: 6,
// numPendingTestSuites: 0,
// numPendingTests: 0,
// numRuntimeErrorTestSuites: 0,
// numTodoTests: 0,
// numTotalTestSuites: 6,
// numTotalTests: 12,

// Our reporter implements only the onRunComplete lifecycle
// function, run after all tests have completed
class SlackReporter{
  async onRunComplete(_, results) {
    // TODO Add Slack webhook trigger
    // console.log(results);

    console.log('Your report is available!');
  }
}

module.exports = SlackReporter